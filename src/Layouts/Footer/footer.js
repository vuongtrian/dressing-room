import React, { Component } from "react";

class FooterComponent extends Component {
  render() {
    return (
      <div className="container d-flex justify-content-center">
        <div className="row"/>
        <div className="card border-0 hovercard">
          <div className="card-info">
            {" "}
            <span className="card-title text-muted">
              @Copyright 2018- CyberSoft.edu.vn - MyClass.vn
            </span>
          </div>
        </div>
      </div>
    );
  }
}

export default FooterComponent;
