import React, { Component } from "react";

class HeaderComponent extends Component {
  render() {
    return (
      <div className="container">
        <div className="row d-felx justify-content-center">
          <div className="card hovercard">
            <div className="card-background"></div>
            <div className="useravatar d-flex justify-content-center">
              <img alt="cybersoft.edu.vn" src="Img/cybersoft.png" />
            </div>
            <div className="card-info">
              {" "}
              <span className="card-title text-muted font-weight-bold">
                CyberSoft.edu.vn - Đào tạo chuyên gia lập trình - Dự án thử đồ
                trực tuyến - Virtual Dressing Room
              </span>
            </div>
          </div>
        </div>
        <hr class="style13"></hr>
      </div>
    );
  }
}

export default HeaderComponent;
