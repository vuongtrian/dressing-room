import React from 'react';
import logo from './logo.svg';
// import './App.css';
import HomeScreen from './Screens/Home/home';

function App() {
  return (
    <div className="App">
      <HomeScreen/>
    </div>
  );
}

export default App;
