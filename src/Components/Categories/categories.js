import React, { Component } from "react";
import { connect } from "react-redux";

class CategoriesComponent extends Component {
  render() {
    return (
      <div className="btn-group d-flex justify-content-center">
        {this.props.categoryList.map((item, index) => (
          <button
            key={index}
            onClick={() => this.chooseCategory(item.type)}
            className={this.props.chooseCategory === item.type ? "btn btn-success m-1" : "btn btn-secondary m-1"}
          >
            {item.showName}
          </button>
        ))}
      </div>
    );
  }
  chooseCategory = (payload) => {
    this.props.dispatch({
      type: "SET_CATEGORY",
      payload,
    });
  };
}

const mapStateToProps = (state) => ({
  categoryList: state.categories,
  chooseCategory: state.choosenCategory,
});
export default connect(mapStateToProps)(CategoriesComponent);
